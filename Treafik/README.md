# Traefik

## Configuration

* traefik.yml : static configuration
* traefik-dyn.yml : dynamique configuration

Le reste des configurations se fait par les __labels__ directement dans le docker compose des stacks (services) deployées.  

## Deploiement

Creation du reseau overlay public
```
docker network create --driver=overlay traefik-public
```

Deploiement
```
docker stack deploy -c traefik-compose.yml traefik
```
