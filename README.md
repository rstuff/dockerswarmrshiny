# Administration du DockerSwarm pour les Apps R Shiny

* [Initialisation du DockerSwarm](#initialisation-docker-swarm)
* [Docker registry (interne au cluster)](#docker-registry)
* [GUI cluster (via portainer)](#portainerd)
* [GitLab Runner local pour le deploiement](#gitlab-runner)
* [Les ShinyProxy](#shinyproxies)
* [Traefik (frontal)](#traefik)
* [Monitoring (via prometheus exporter)](#prometheus)
* [Configuration system hote](#configuration-system)

## Commandes de bases

```
#info docker
docker info
# liste des nodes
docker node ls
# to drain a node
docker node update --availability drain <id_node>

# Liste des stack
docker stack ls
# Listes service d'une stack
docker stack services <nom_stack>

# list services
docker service ls
# creation service
docker service create --name <nom_service> <image_docker> <CMD> <ARGS>
# update a service
docker service update <truc a update> <nom_service>
# arret service
docker service rm <nom_service>|<ID_service>
# list tack service
docker service ls <nom_service>
# inspect a service
docker service inspect --pretty <nom_service>


# ajout stack
docker stack deploy -c <docker-compose.yml> <nom_stack>
# arret stack
docker stack rm <nom_stack>
# info stack
docker stack ps <stack_name>
```

## Initialisation Docker swarm

Sur le Manager:
```
# active mode swarm sur la manager
docker swarm init --advertise-addr <IP_MANAGER>
# pour avoir le token de registration a executer sur les nodes
docker swarm join-token worker
```

Sur les workers (commande donnée par la commande précédentes):
```
docker swarm join --token <TOKEN> <IP_MANAGER:PORT>
```

On va ajouter un manager et mettre des labels pour les identifiers [non utilisé]:
```
docker node promote DSNode1
docker node update --label-add position=nord DSMaster
```

Dorenavant les services qui doivent tourner sur DSMaster (position=nord) pourront l'etre via les contraintes de déploiement.
```
placement:
  constraints:
    - node.labels.position == nord
```

### Troubleshot

Bon docker swarm c'est bien mais il y a pas mal de problème.

#### Service qui tourne encore...

Pour tuer les services lier a shinyproxy (sp-service est un nom generique pour les services, normalement il y a que du shinyproxy avec ce nom)
```
docker service rm $(docker service ls | grep sp-service | cut -d " " -f 1)
```

#### Worker Down

Redémarrer docker ou la VM...

#### Problème de creation de container

Limite du nombre de processus. Etendre la limit dans /ets/security/limit.conf .  
Mettre a jour les certificats en manager et nodes `docker swarm ca --rotate`

## Docker registry

Il sert de dèpôt local au cluster pour nos images docker à deployer.

Céation du network pour le registry
```
docker network create -d overlay --attachable --internal registrynet
```

Lancement du service sur __Manager__  
```
docker stack deploy -c registry-compose.yml localregistry
```

## Portainerd

Ne pas mettre en prod ! Pour le debug seulement!  

```
docker stack deploy -c portainer-agent-stack.yml portainer
```

## GitLab Runner

Pour le deploiement automatique sur le cluster.  
Lancement de gitlab-runner sur la manager en standalone pour que les runners soient persistant sur le manager.
```
docker run -d \
    --name gitlab-runner \
    --restart always \
    --network registrynet \
    -v /srv/dockerswarmmanager/gitlab-runner:/etc/gitlab-runner \
    -v /var/run/docker.sock:/var/run/docker.sock \
    gitlab/gitlab-runner:latest
```

## ShinyProxies

### Shinyproxy

> Le fichier __shinyproxy-compose.yml__ est a jour dans les depots respectifs. cela signifie qu'on a pas a faire cela ici.

Creation du reseau
```
docker network create -d overlay --attachable --subnet=10.10.0.0/16 --gateway=10.10.0.2 shinyproxynet
```

Deploiement
```
docker stack deploy -c <fichier compose du shinyproxy>.yml shinyproxy_public
```

## Traefik

> On utilise que le reseau public pour le moment

Création de deux reseaux public et privee

```
docker network create -d overlay traefik-public
docker network create -d overlay traefik-backend
```

> Le service (stack) Traefik utilise le reseau traefik-public.  
En interne Traefik utilise treafik-backend

```
cd Treafik
docker stack deploy -c traefik-compose.yml traefik
```
Le fichier _traefik.yml_ sert a configurer le mode static.  
Le fichier _traefik-dyn.yml_ sert a configurer le mode dynamique.
Le reste de la configuration se fait via les labels des services (containers).

## Monitoring via Prometheus

Le reseau pour le monitoring:
```
docker network create -d overlay --attachable monitor-net
```

### Prometheus

Prometheus est accesible depuis le port 9090.  
> interdire les droits d'acces au port depuis l'exterieur.

Deploiement:
```
cd Prometheus
docker stack deploy -c prometheus-stack.yml monitor
```

L'url d'acces pour un autre prometheus : http://<IP>:9090/federate



## Configuration system

Certains fichiers de configuration system sont disponibles dans [conf_sys].  
* iptables rules
* logwatch
* fail2ban (ajout traefik)
