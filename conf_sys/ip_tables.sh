#!/bin/bash
systemctl stop fail2ban
iptables -L
iptables -F INPUT
iptables -F OUTPUT

### System host
iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
iptables -A INPUT -s 127.0.0.0/8 -d 127.0.0.0/8 -i lo -j ACCEPT

# ssh
iptables -A INPUT -p tcp -i ens19 --dport ssh -s 147.100.14.142 -j ACCEPT
iptables -A INPUT -p tcp -i ens19 --dport ssh -s 147.100.14.117 -j ACCEPT
# http https (inutile fait par docker-user)
iptables -A INPUT -p tcp -m multiport --dports 80,443 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT

# docker registry
iptables -A INPUT -p tcp -i ens19 --dport 5000 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT

# docker swarm
iptables -A INPUT -p tcp -i ens19 --dport 2376 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -p tcp -i ens19 --dport 2377 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -p udp -i ens19 --dport 4789 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -p tcp -i ens19 --dport 7946 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -p udp -i ens19 --dport 7946 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT


 # mail
iptables -A OUTPUT -m owner --gid-owner postfix -p tcp -m tcp --dport 25 -j ACCEPT
iptables -A OUTPUT -m owner --uid-owner root -p tcp -m tcp --dport 25 -j ACCEPT
iptables -A OUTPUT -p tcp -m tcp --dport 25 -j REJECT --reject-with icmp-port-unreachable

# icmp
iptables -A INPUT -p icmp -j ACCEPT

iptables -A INPUT -j DROP

### System docker ingress
### Docker prend le dessus sur INPUT
### pour les ports publies en 0.0.0.0:port il faut ajouter des regles pour bloquer le port depuis lexterieur ou autre.
### avec docker ingress on ne peut pas publier un port en localhost il est forcement accessible depuis tous les nodes.
### pour eviter des problemes avec les regles de docker, on rejette les ports non accessibles et on accept le reste (on devrait faire le contraire, autorise les ports et rejetter tous le reste)
iptables -F DOCKER-USER
#iptables -A DOCKER-USER -m conntrack --ctstate ESTABLISHED,RELATED -j RETURN

##http, https
#iptables -A DOCKER-USER -p tcp -m multiport --dports 80,443 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
# monitor
iptables -A DOCKER-USER -p tcp -i ens18 --dport 9090 -m conntrack --ctstate NEW,ESTABLISHED -j DROP
# docker registry
iptables -A DOCKER-USER -p tcp -i ens18 --dport 5000 -m conntrack --ctstate NEW,ESTABLISHED -j DROP

iptables -A DOCKER-USER -j RETURN

netfilter-persistent save
#iptables-save > /etc/iptables/rules.v4
iptables -L

systemctl start fail2ban

