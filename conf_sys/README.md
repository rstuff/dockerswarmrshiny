# Config systeme

## Problème de race entre udev et dockerd

Mac address erreur.  
Ajouter le fichier [99-default.link] dans /etc/systemd/network/

Et aussi trouvé ceci :
Ajouter le fichier 01-net-setup-link.rules dans /etc/udev/rules.d/

## Erreur d'acces https local registry

Le registry n'est accessible uniquement que dans le swarm. Le https n'est pas activé, il faut specifier a dockerd que l'access au registry se fait en http pas en https.

Copier/ajouter le fichier [daemon.json] dans /etc/docker/

