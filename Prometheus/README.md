# Monitoring avec Prometheus

Il faut faire tourner chaque exporter sur chaque node (du swarm) et dire a prometheus de trouver les exporter dynamiquement en utilisant le dns.

Fichier de configuration : _prometheus.yml_

Le reste est configuré dans _prometheus-stack.yml_

## network
```
docker network create -d overlay --attachable monitor-net
```

## Deploiement

```
docker stack deploy -c prometheus-stack.yml monitor
```

## Services inclus

### node exporter

Repertoire node\_exporter contient le entry point du container pour ajouter une meta donnée pour identifier les nodes.

### cadvisor

Fonctionnement normale via le container.

### traefik

Prometheus monitor traefik via dns dans docker swarm sur le port 8080

### dockerd-exporter

Non deployé - toujours en version de test coté docker.

### Prometheus

Prometheus est configuré sur le master est le port 9090.  
> Interdire l'acces au port pour les machines non autorisé.

* URL http://<IP>:9090
* federate : http://<IP>:9090/federate

